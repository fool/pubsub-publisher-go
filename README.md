# Google PubSub message publisher

Quickly pubsub any message to a topic

# Building
```make```

# Authenticating
You can log in using `gcloud auth` CLI or provide a JSON file of credentials with `--credentials`

Example `gcloud-auth`:
```bash
gcloud auth application-default login
```

# Developing
- Set your google json credentials file in common.sh
- Run this to build and execute
```
./run.sh  --project-id <project> --topic-id <topic> --message <message>
```
