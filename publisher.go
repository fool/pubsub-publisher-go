package main

import (
	"encoding/json"
	"os"
    "flag"
    "strings"
    "fmt"
    "errors"
	"cloud.google.com/go/pubsub"
	"golang.org/x/net/context"
	"github.com/op/go-logging"
	"google.golang.org/api/option"
)

var log = logging.MustGetLogger("publisher")
var logFormat = logging.MustStringFormatter(
	`%{level} [%{time:15:04:05.000}] %{message}`,
)


/**
 * Make a pubsub client
 *
 * @param credentials Optional file name containing JSON google credentials. Can be empty to use
 *                    background authentication
 * @param ctx         ???
 * @param projectId   String project id for google cloud
 * @return            the client
 */
func create_client(credentials *string, ctx context.Context, projectId *string) *pubsub.Client {
    if *credentials != "" {
        log.Debug("Using credentials from file", *credentials)
        client, err := pubsub.NewClient(ctx, *projectId, option.WithCredentialsFile(*credentials))
        if err != nil {
            log.Error("Error creating a pub sub client:", err)
            os.Exit(6)
        }
        return client
    } else {
        client, err := pubsub.NewClient(ctx, *projectId)
        if err != nil {
            log.Error("Error creating a pub sub client:", err)
            if strings.Contains(err.Error(), "could not find default credentials") {
                log.Error("You need to login using 'gcloud auth' command line or pass in a JSON credentials file using --credentials <file>")
            }
            os.Exit(7)
        }
        return client
    }
}

/**
 * Parse a json string into a map of string => string
 * @param attributes JSON string
 * @return map[string]string on success
 *         err describing how this didn't parse correctly otherwise
 */
func parseAttributes(attributes *string) (map[string]string, error) {
    var jsonHolder interface{}
    var m = make(map[string]string)

    err := json.Unmarshal([]byte(*attributes), &jsonHolder)
    if err != nil {
        return nil, err
    }

    for k, v := range jsonHolder.(map[string]interface{}) {
        switch vv := v.(type) {
        case string:
            m[k] = vv
        default:
            return nil, errors.New(fmt.Sprintf("Attribute '%s' does not have a string value", k))
        }
    }
    return m, nil
}


func main() {
    backend := logging.AddModuleLevel(logging.NewBackendFormatter(logging.NewLogBackend(os.Stdout, "", 0), logFormat))
    DefaultLogLevel := "info"

    projectId := flag.String("project-id", "", "Project ID (required)")
    pubSubTopicId := flag.String("topic-id", "", "PubSub Topic ID (required)")
    message := flag.String("message", "", "Message to send (required)")
    attributes := flag.String("attributes", "", "Optional PubSub Attributes in JSON format. Must be String keys and String values")
    credentials := flag.String("credentials", "", "Optional google JSON credentials file. If not provided you need to use 'gcloud auth application-default login'")
    logLevelString := flag.String("log-level", DefaultLogLevel, "Optional log level. Choices: 'debug', 'info', 'notice', 'warning', 'error', 'critical'")
    flag.Parse()

    parseError := false

    logLevel, err := logging.LogLevel(*logLevelString)
    if err != nil {
        logging.SetBackend(backend)
        backend.SetLevel(logging.INFO, "")
        log.Error("Unable to parse log level:", *logLevelString, "Must be one of: 'debug', 'info', 'notice', 'warning', 'error', 'critical'")
        os.Exit(3)
    }
    backend.SetLevel(logLevel, "")
    logging.SetBackend(backend)


    if *projectId == "" {
        log.Error("--project-id <proj id> is required")
        parseError = true
    }
    if *pubSubTopicId == "" {
        log.Error("--topic-id <topic> is required")
        parseError = true
    }
    if *message == "" {
        log.Error("--message <msg> is required")
        parseError = true
    }
    var attributesJson map[string]string
    if *attributes != "" {
        attributesJson, err = parseAttributes(attributes)
        if err != nil {
            log.Error("Unable to parse attributes as json", err)
            parseError = true
        }
    } else {
        attributesJson = nil
    }

    if parseError {
        os.Exit(3)
    }

    ctx := context.Background()
    log.Debug("Creating new pubsub client")
    client := create_client(credentials, ctx, projectId)

    log.Debug("Fetching topic", *pubSubTopicId)
    topic := client.Topic(*pubSubTopicId)

    byteMessage := []byte(*message)

    log.Debug(fmt.Sprintf("Publishing message (%d bytes)...", len(byteMessage)))
    _, err = topic.Publish(ctx, &pubsub.Message{
        Data: byteMessage,
        Attributes: attributesJson,
    }).Get(ctx)
    if err == nil {
      log.Info("Published message successfully")
    } else {
      log.Error("Error publishing: ", err)
      os.Exit(15)
    }
}
