#!/bin/bash

function log() {
	local level="$1"
	local message="$2"
	echo "$level [$(date +"%Y-%m-%dT%H:%M:%S%:z")] $message"
}

GOOGLE_APPLICATION_CREDENTIALS="put-your-google-cred-file-here.json"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
GOPATH="$DIR"
